import xlrd
from openpyxl.utils import coordinate_from_string
from PyQt5 import  QtWidgets, QtGui, uic
from PyQt5.QtCore import pyqtSignal, QThread, QElapsedTimer
import sys
import os
from uuid import getnode as get_mac

mac = hex(get_mac())

class PopMessage(QtWidgets.QMessageBox):
    def __init__(self):
        QtWidgets.QMessageBox.__init__(self)
        self.move(QtWidgets.QApplication.desktop().screen().rect().center() - self.rect().center())

    def warning_mess(self, text1, text2):
        QtWidgets.QMessageBox.warning(self, text1, text2, QtWidgets.QMessageBox.Yes)

    def info_mess(self, text1, text2):
        QtWidgets.QMessageBox.information(self, text1, text2, QtWidgets.QMessageBox.Yes)

    def critical_mess(self, text1, text2):
        QtWidgets.QMessageBox.critical(self, text1, text2, QtWidgets.QMessageBox.Yes)


class TasksThread1(QThread):
    valSignal = pyqtSignal(float)
    def __init__(self):
        QThread.__init__(self)
        self.file1 = ""
        self.file2 = ""
        main_window.loadSignal.connect(self.loadSignaVar)
        main_window.saveSignal.connect(self.saveSignalVar)
        self.nrPers = 0
        self.calcIntreg = 0
        self.endRowValue = 0
        self.timer = QElapsedTimer()

    def loadSignaVar(self, inputText):
        self.file1 = inputText

    def saveSignalVar(self, inputText):
        self.file2 = inputText

    def getStartRow(self):
        """Returneaza celula de inceput dar nu returneaza valoarea ei, valoare returnata
        este statica, tabelul incepe din acelasi loc de fiecare data"""
        return 15

    def getEndRow(self):
        """Returneaza celula de sfarsit dar nu returneaza valoarea ei."""
        self.endRowValue = self.first_sheet.nrows - 23

    def getOneRecord(self, row_in, column_in):
        """Functia returneaza informatia pentru o singura persoana in functie,
        de coordonatele primite"""
        id = self.first_sheet.row_values(row_in, column_in, column_in + 1)
        nume = self.first_sheet.row_values(row_in, column_in + 3, column_in + 4)
        cnp = self.first_sheet.row_values(row_in + 1, column_in + 3, column_in + 4)
        functia = self.first_sheet.row_values(row_in + 2, column_in + 3, column_in + 4)
        grad = self.first_sheet.row_values(row_in + 3, column_in + 3, column_in + 4)
        vechime_invatamant = self.first_sheet.row_values(row_in + 4, column_in + 3, column_in + 4)
        functie_de_baza = self.first_sheet.row_values(row_in + 1, column_in + 12, column_in + 13)
        norma_calcul = self.first_sheet.row_values(row_in + 2, column_in + 12, column_in + 13)
        norma_de_baza = self.first_sheet.row_values(row_in + 3, column_in + 12, column_in + 13)
        norma_pco = self.first_sheet.row_values(row_in + 4, column_in + 12, column_in + 13)
        norma_cumul = self.first_sheet.row_values(row_in + 5, column_in + 12, column_in + 13)
        norma_did_pco = self.first_sheet.row_values(row_in, column_in + 18, column_in + 19)
        sal_baza = self.first_sheet.row_values(row_in, column_in + 22, column_in + 23)
        sal_baza_tarifar_normat = self.first_sheet.row_values(row_in + 1, column_in + 22, column_in + 23)
        return (int(id[0]), nume[0], cnp[0], functia[0], grad[0], vechime_invatamant[0],
                functie_de_baza[0], norma_calcul[0], norma_de_baza[0], norma_pco[0],
                norma_cumul[0], norma_did_pco[0], sal_baza[0], sal_baza_tarifar_normat[0])

    def setTemplateFile(self, line):
        """Salveaza in fisierul csv fiecare record extras din xlsx"""
        with open(self.file2, 'a') as f:
            f.write(line)
        f.close()

    def executeCode(self):
        """Ruleaza scriptul si trimite informatiile spre output"""
        for x in range(self.getStartRow(), self.endRowValue, 8):
            self.valSignal.emit(self.calcIntreg)
            tupRecord = self.getOneRecord(x, 1)
            for records in tupRecord:
                self.setTemplateFile('{},'.format(records))
            self.setTemplateFile('\n')
        self.valSignal.emit(self.calcIntreg)

    def run(self):
        self.timer.start()
        self.book = xlrd.open_workbook(self.file1)
        self.first_sheet = self.book.sheet_by_index(0)
        self.getEndRow()
        self.nrPers = (int(((self.endRowValue - self.getStartRow()) / 8) + 1))
        self.calcIntreg = (100 / self.nrPers) + 1
        with open(self.file2, 'w') as f:
            f.write('NrCrt., Nume Prenume, CNP, Functia, Grad didactic, Vechime invatamant, Functie de baza,'
                    'Norma calcul, Norma de baza, Norma PCO, Norma cumul, Norma did PCO, Sal.Baza, Sal.Baza tarifar normat\n')
        f.close()
        self.executeCode()


class MainWindow(QtWidgets.QMainWindow):
    loadSignal = pyqtSignal(str)
    saveSignal = pyqtSignal(str)
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        file_path = os.path.abspath('edusalGui.ui')
        uic.loadUi(file_path, self)
        self.move(QtWidgets.QApplication.desktop().screen().rect().center() - self.rect().center())
        self.outputBtn.pressed.connect(self.getOutputFileLocation)
        self.loadBtn.pressed.connect(self.getLoadFileLocation)
        self.startBtn.pressed.connect(self.executeThreadRun)
        self.outputFile = None
        self.loadFile = None

    def executeThreadRun(self):
        global objectThred
        objectThred.start()
        objectThred.valSignal.connect(self.loadProgress)
        objectThred.finished.connect(self.printStatus)

    def printStatus(self):
        popup.info_mess("Status Info", "Conversie finalizata cu succes!")

    def statusBarMessage(self, text):
        self.statusBar.showMessage(text)

    def getOutputFileLocation(self):
        self.outputFile = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Output csv File', '/path/to/default/', '*.csv')
        self.lineEdit_2.setText(self.outputFile[0])
        self.saveSignal.emit(self.outputFile[0])

    def getLoadFileLocation(self):
        self.loadFile = QtWidgets.QFileDialog.getOpenFileName(self, 'Load xlsx File', '/path/to/default/', '*.xlsx')
        self.lineEdit.setText(self.loadFile[0])
        self.loadSignal.emit(self.loadFile[0])

    def loadProgress(self, val):
        initVal = self.progressBar.value()
        self.progressBar.setValue(val+initVal)

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow()
    objectThred = TasksThread1()
    popup = PopMessage()
    main_window.show()
    sys.exit(app.exec_())